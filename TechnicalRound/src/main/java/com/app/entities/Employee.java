package com.app.entities;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Employee {
	
	@Id
	@NotNull(message = "Please Provde Employee Id")
    private Integer eid;
	
	@Column
	@NotNull(message = "Please Provide First Name")
    private String fname;
	
	@Column
	@NotNull(message = "Please Provide Last Name")
    private String lname;
	
	@Column
	@NotNull(message = "Please Provide Email")
    private String email;
	
	@Column
	@NotNull(message = "Please Provide Phoneno")
    private Long phoneno;
	
	@Column
	@NotNull(message = "Please Provide Date of Joining")
    private String doj;
	
	@Column
	@NotNull(message = "Please Provide Salary")
    private long salary;

	public Integer getEid() {
		return eid;
	}

	public void setEid(Integer eid) {
		this.eid = eid;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getPhoneno() {
		return phoneno;
	}

	public void setPhoneno(Long phoneno) {
		this.phoneno = phoneno;
	}

	public String getDoj() {
		return doj;
	}

	public void setDoj(String doj) {
		this.doj = doj;
	}

	public long getSalary() {
		return salary;
	}

	public void setSalary(long salary) {
		this.salary = salary;
	}
	
	


}
