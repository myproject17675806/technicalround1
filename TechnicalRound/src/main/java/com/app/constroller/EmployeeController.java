package com.app.constroller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.entities.Employee;
import com.app.service.EmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;
	
	@PostMapping("/insertEmployee")
	public ResponseEntity<?> saveEmployee(@Valid @RequestBody Employee employee)
	{
		ResponseEntity<?> resp=null;
		try
		{
			Integer eid=employeeService.saveEmployee(employee);
			resp=new ResponseEntity<String>("Employee Data Saved With Id:"+eid,HttpStatus.OK);
			
		}
		catch(Exception e)
		{
			resp=new ResponseEntity<String>("Unable to save employee data",HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return resp;
	}
	
	@GetMapping("/getAllEmployees")
	public ResponseEntity<?> getAllEmployees()
	{
		
		ResponseEntity<?> resp=null;
		
		try
		{
		 	ArrayList<com.app.dtos.Employee> employees=new ArrayList<>();
			 
		    List<Employee> list= employeeService.getAllEmployees();
		    
		    for(Employee emp:list)
		    {
		    	com.app.dtos.Employee e=new com.app.dtos.Employee();
		    	e.setEid(emp.getEid());
		    	e.setFname(emp.getFname());
		    	e.setLname(emp.getLname());
		    	long annualSalary=emp.getSalary()*12;
		    	e.setAnuualSalary(annualSalary);
		    	long tax=0;
				if(annualSalary<=250000)
				{
					tax=0;
				}
				else if(annualSalary>250000 && annualSalary<=500000)
				{
					tax = (annualSalary*5)/100;
				}
				else if(annualSalary>500000 && annualSalary<=1000000)
				{
					tax = (annualSalary*10)/100;
				}
				else if(annualSalary>1000000)
				{
					tax = (annualSalary*20)/100;	
				}
		    	e.setTaxAmount(tax);
		    	
		    	long cessAmount=0;
				if(annualSalary>2500000)
				{
					cessAmount=(annualSalary*2)/100;
				}
				
				e.setCessAmount(cessAmount);
				
				employees.add(e);
		    	
		    }
		    
		    resp=new ResponseEntity<List<com.app.dtos.Employee>>(employees,HttpStatus.OK);
		}
		catch(Exception e)
		{
			
			resp=new ResponseEntity<String>("Unable to Fetch Employees Data",HttpStatus.INTERNAL_SERVER_ERROR);
			
		}
return resp;
		
	}
	
}
