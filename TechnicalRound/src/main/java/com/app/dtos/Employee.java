package com.app.dtos;

public class Employee 
{
	
	
	
   private Integer eid;
	
	
	private String fname;
	
	
	private String lname;
	
	
	private long anuualSalary;
	
	private long taxAmount;
	
	private long cessAmount;

	public Integer getEid() {
		return eid;
	}

	public void setEid(Integer eid) {
		this.eid = eid;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public long getAnuualSalary() {
		return anuualSalary;
	}

	public void setAnuualSalary(long anuualSalary) {
		this.anuualSalary = anuualSalary;
	}

	public long getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(long taxAmount) {
		this.taxAmount = taxAmount;
	}

	public long getCessAmount() {
		return cessAmount;
	}

	public void setCessAmount(long cessAmount) {
		this.cessAmount = cessAmount;
	}

	

}
