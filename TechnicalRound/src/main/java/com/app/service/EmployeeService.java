package com.app.service;

import java.util.List;

import com.app.entities.Employee;



public interface EmployeeService {

	
	public Integer saveEmployee(Employee employee);
	public List<Employee> getAllEmployees();
	
	
}
